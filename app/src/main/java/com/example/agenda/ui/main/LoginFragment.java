package com.example.agenda.ui.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.agenda.R;

public class LoginFragment extends Fragment {

    LogViewModel mViewModel;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.toLoginBtn).setOnClickListener(this::login);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LogViewModel.class);
        mViewModel.loading.observe(getViewLifecycleOwner(), this::onLoadingChanged);
        mViewModel.error.observe(getViewLifecycleOwner(), this::onErrorFired);
        mViewModel.logged.observe(getViewLifecycleOwner(), this::onLogChanged);
    }

    private void login(View view) {
        boolean valid= validate();
        if(valid){
            mViewModel.login(getUserName(), getPassword());
        }
    }


    ProgressDialog dialog;
    private void onLoadingChanged(Boolean aBoolean) {
        if(aBoolean) {
            dialog = ProgressDialog.show(getContext(), "",
                    R.string.loadingmessage, true);
            dialog.show();
        } else {
            if(dialog != null)
                dialog.dismiss();
        }
    }

}