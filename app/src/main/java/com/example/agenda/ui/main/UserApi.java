package com.example.agenda.ui.main;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserApi {

    Call<UserSession> login(@Path("username") String username);

    Call<UserSession> register(@Path("username") String username);

}
