package com.example.agenda.ui.main;


public class UserSession {
    String authToken;
    String error;

    public String getAuthToken() {
        return authToken;
    }

    public String getError() {
        return error;
    }

    public boolean isLogged(){
        return authToken!=null;
    }
}
