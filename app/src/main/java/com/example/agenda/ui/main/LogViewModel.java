package com.example.agenda.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class LogViewModel extends ViewModel {
    public static final String UNKNOWN_ERROR = "unknown error";
    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();


    UserApi userApi = UserApiProvider.getUserApi();
    public void login(String username, String password){
        loading.postValue(true);
        userApi.login(username).enqueue(
                new Callback<UserSession>() {
                    @Override
                    public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                        UserSession userSession = response.body();
                        if(userSession==null){
                            error.postValue(UNKNOWN_ERROR);
                        } else if(userSession.isLogged()){
                            logged.postValue(true);
                        } else {
                            error.postValue(userSession.getError());
                        }
                        loading.postValue(false);
                    }

                    @Override
                    public void onFailure(Call<UserSession> call, Throwable t) {
                        error.postValue(UNKNOWN_ERROR);
                        loading.postValue(false);
                        t.printStackTrace();
                    }
                }
        );
    }

    public MutableLiveData<Boolean> getLogged() {
        return logged;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<String> getError() {
        return error;
    }
}